// soal 1
console.log("============ soal 1 ============");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for (let i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan[i]);
}

// soal 2
console.log("============ soal 2 ============");

function introduce(data) {
    return `Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobby yaitu ${data.hobby}`;
}
 
var data = {name : "Aghniya" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
 
var perkenalan = introduce(data);
console.log(perkenalan);

// soal 3
console.log("============ soal 3 ============");

function hitung_huruf_vokal(kata) {
    var kata = kata.toLowerCase();
    var vokal = "aiueo";
    var jumlah = 0;
    for (let i = 0; i < kata.length; i++) {
        if (vokal.includes(kata[i])) {
            jumlah++;
        }
    }
    return jumlah;
}
var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1 , hitung_2);

// soal 4
console.log("============ soal 4 ============");

function hitung(number) {
    return (number * 2) - 2;
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8