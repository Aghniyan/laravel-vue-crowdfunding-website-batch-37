export const UsersComponent = {
    template: `
    <template>
        <div class="usersList">
            <ul>
                <li v-for="(user , index) in reverseUsers" :key="index">
                    {{ user.name }} ||
                    <button @click="$emit('edit-user',index)">Edit</button>
                    <button @click="$emit('delete-user',index)">Delete</button>
                </li>
            </ul>
        </div>
    </template>
    `,
    props: ['reverse-users']
}