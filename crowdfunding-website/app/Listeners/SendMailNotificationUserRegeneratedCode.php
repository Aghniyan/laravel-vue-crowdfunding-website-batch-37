<?php

namespace App\Listeners;

use App\Events\UserRegeneratedCodeEvent;
use App\Mail\UserRegeneratedCodeMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendMailNotificationUserRegeneratedCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegeneratedCodeEvent  $event
     * @return void
     */
    public function handle(UserRegeneratedCodeEvent $event)
    {
        Mail::to($event->user->email)->send(new UserRegeneratedCodeMail($event->user));
    }
}
