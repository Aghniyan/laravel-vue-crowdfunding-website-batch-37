<?php

namespace App;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    use UuidTrait;

    protected $fillable = [
        'otp',
        'user_id',
        'valid_until',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
