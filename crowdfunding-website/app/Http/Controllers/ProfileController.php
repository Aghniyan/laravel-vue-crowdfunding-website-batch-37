<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProfileController extends Controller
{
    public function show(Request $request)
    {
        $user = auth()->user();
        $data['user'] = $user;
        return response()->json([
            'response_code' => '00',
            'response_message' => 'profile berhasil ditampilkan',
            'data' => $data,
        ], 200);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'photo_profile' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $user = auth()->user();
        $user->name = $request->name;
        if ($files = $request->file('photo_profile')) {
            $path = "image/user/photo-profile";
            $file_name = Str::slug($user->name) . '_' . time() . '.' . $files->getClientOriginalExtension();
            $file = $files->move(public_path($path), $file_name);
            if ($file) {
                $user->photo_profile = $path.'/'.$file_name;
            }
        }
        $user->save();
        $data['user'] = $user;
        return response()->json([
            'response_code' => '00',
            'response_message' => 'profile berhasil di update',
            'data' => $data,
        ], 200);
    }
}
