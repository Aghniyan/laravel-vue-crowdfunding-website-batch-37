<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'email user tidak ditemukan',
            ], 404);
        }

        $user->update([
            'password' => bcrypt($request->password),
        ]);

        $data['user'] = $user;
        return response()->json([
            'response_code' => '00',
            'response_message' => 'password berhasil di update',
            'data' => $data,
        ], 200);
    }
}
