<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait UuidTrait
{
    function getIncrementing()
    {
        return false;
    }

    function getKeyType()
    {
        return 'string';
    }

    public static function bootUuidTrait()
    {
        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
