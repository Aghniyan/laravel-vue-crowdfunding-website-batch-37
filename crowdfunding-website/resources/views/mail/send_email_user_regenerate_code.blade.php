<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body align="center">
    <h1>Congrats, {{ $name }} !!!</h1>
    <p>You have been regenerated your otp</p>
    <p>this is your otp: <strong>{{ $otp }}</strong></p>
    <p>Please input this otp to verify your account until 5 minutes</p>
</body>
</html>
