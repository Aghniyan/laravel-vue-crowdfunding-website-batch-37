export default {
    namespaced: true,
    state: {
        campaign_image: "https://static.wixstatic.com/media/157553_79f4b62283d84c648705f25b2190caf4.png",
        blog_image: "https://th.bing.com/th/id/OIP.tB4k2osHKMjFb9403YKoJAHaFR?pid=ImgDet&rs=1",
        user_image: "https://i1.wp.com/norrismgmt.com/wp-content/uploads/2020/05/24-248253_user-profile-default-image-png-clipart-png-download.png?fit=300%2C262&ssl=1",
    },
    mutations: {

    },
    actions: {

    },
    getters: {
        campaign_image: state => state.campaign_image,
        blog_image: state => state.blog_image,
        user_image: state => state.user_image
    }
}
