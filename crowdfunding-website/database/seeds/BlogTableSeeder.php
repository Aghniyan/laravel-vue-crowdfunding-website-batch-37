<?php

use App\Blog;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=0; $i < 10; $i++) {
            Blog::create([
                'title' => "Blog ". Str::random(10),
                'description' => "Blog Deskripsi ". Str::random(100),
            ]);
        }
    }
}
