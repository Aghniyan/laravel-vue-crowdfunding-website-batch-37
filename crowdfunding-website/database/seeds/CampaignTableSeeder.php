<?php

use App\Campaign;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class CampaignTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) {
            # code...
            Campaign::create([
                'title' => Str::random(10),
                'description' => Str::random(100),
                'address' => Str::random(20),
                'required' => 2000,
                'collected' => 0,
            ]);
        }
    }
}
