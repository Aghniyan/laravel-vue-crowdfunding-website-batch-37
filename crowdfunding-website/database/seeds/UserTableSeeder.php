<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin_id = Role::where('name', 'admin')->first();
        $role_user_id = Role::where('name', 'user')->first();
        User::create([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => $role_admin_id->id,
        ]);

        User::create([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'role_id' => $role_user_id->id,
        ]);

        User::create([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'role_id' => $role_user_id->id,
        ]);

    }
}
