<?php

trait Hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo "$this->nama sedang $this->keahlian";
    }
}

abstract class Fight {
    use Hewan;
    public $attackPower, $defencePower;

    public function serang($hewan)
    {
        echo "$this->nama sedang menyerang $hewan->nama <br>";
        $this->diserang($hewan);
    }

    public function diserang($hewan)
    {
        $this->darah -= ($hewan->attackPower/$hewan->defencePower);
        echo "$hewan->nama sedang diserang $this->nama <br>";
    }

    protected function getInfo()
    {
        echo "Nama : $this->nama <br>";
        echo "Darah : $this->darah <br>";
        echo "Jumlah Kaki : $this->jumlahKaki <br>";
        echo "Keahlian : $this->keahlian <br>";
        $this->atraksi();
    }

    abstract public function getInfoHewan();
}

class Elang extends Fight{
    public function __construct($string)
    {
        $this->nama = $string;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Elang <br>";
        $this->getInfo();
    }
}

class Harimau extends Fight{
    public function __construct($string)
    {
        $this->nama = $string;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Harimau <br>";
        $this->getInfo();
    }
}

$elang = new Elang("Elang");
$harimau = new Harimau("Harimau");
$elang->getInfoHewan();
echo "<br>";
$harimau->getInfoHewan();

echo "<br> ======== attack ======== <br>";
$elang->serang($harimau);

$harimau->serang($elang);

echo "<br> ======== info ======== <br>";

$elang->getInfoHewan();
echo "<br>";
$harimau->getInfoHewan();
?>