// Soal 1
console.log("============ soal 1 ============");
const luasPersegi = (panjang, lebar) => {
    return panjang * lebar;
}
const kelilingPersegi = (panjang, lebar) => {
    return (panjang + lebar) * 2;
}
console.log("Luas Persegi : " + luasPersegi(5, 5));
console.log("Keliling Persegi : " + kelilingPersegi(5, 5));

// Soal 2
console.log("============ soal 2 ============");
const newFunction = (firstName, lastName) => {
    console.log(firstName + " " + lastName)
}

//Driver Code 
newFunction("William", "Imoh");

// Soal 3
console.log("============ soal 3 ============");
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

// Soal 4
console.log("============ soal 4 ============");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

// Soal 5
console.log("============ soal 5 ============");
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet}`;
console.log(before);

